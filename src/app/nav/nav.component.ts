import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from '../auth.service';


@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  
  toRegister(){
    this.router.navigate(['/register']);
  }

  toCodes(){
    this.router.navigate(['/codes']);
  }

  toUsertodos(){
    this.router.navigate(['/usertodos']);
  }

  logout(){
    this.authService.logout()
    .then(user => {
      this.router.navigate(['login'])}).catch(err=>{console.log(err)})
  }

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

}
