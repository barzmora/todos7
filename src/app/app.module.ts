import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { TodosComponent } from './todos/todos.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CodesComponent } from './codes/codes.component';
import { TodoComponent } from './todo/todo.component';
import {Routes, RouterModule } from '@angular/router';

import {environment} from '../environments/environment';

//material design imports
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';

//fire base imports
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { UsertodosComponent } from './usertodos/usertodos.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    TodosComponent,
    LoginComponent,
    RegisterComponent,
    CodesComponent,
    TodoComponent,
    UsertodosComponent
  ],
  imports: [
    MatButtonModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    FormsModule,
    MatCardModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      {path:'', component:TodosComponent},
      {path:'register', component:RegisterComponent},
      {path:'usertodos', component:UsertodosComponent},
      {path:'login', component:LoginComponent},
      {path:'codes', component:CodesComponent},
      {path:'**', component:TodosComponent}

    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
