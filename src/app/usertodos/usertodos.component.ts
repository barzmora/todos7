import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase,AngularFireList} from '@angular/fire/database';

@Component({
  selector: 'usertodos',
  templateUrl: './usertodos.component.html',
  styleUrls: ['./usertodos.component.css']
})
export class UsertodosComponent implements OnInit {

  todos = [];

user = 'jack';

  constructor(private db:AngularFireDatabase) { }

  changeUser(){
    this.db.list('/users/' + this.user + '/todos').snapshotChanges().subscribe(
      todos => {
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y['$key'] = todo.key;
            this.todos.push(y);
                  }
        )
      }
    )
  }

  
  ngOnInit() {
    this.db.list('/users/' + this.user + '/todos').snapshotChanges().subscribe(
      todos => {
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y['$key'] = todo.key;
            this.todos.push(y);
                  }
        )
      }
    )
  }
  todoText;
  showText(text){
    this.todoText=text;
  }
}
