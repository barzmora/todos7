import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

  email: string;
  password: string;
  error='';

signIn(){
  console.log("sign in clicked " + this.email + ' '  + ' ' + this.password);
  this.authService.signin(this.email,this.password)
   .then(user => {
        this.router.navigate(['/'])
      }).catch(err => {
        this.error = err;
        console.log(err);
        })
}

}
