import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Router} from "@angular/router";
import {ErrorStateMatcher} from '@angular/material/core';




@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }

  email: string;
  password: string;
  name: string;
  error='';

signUp(){
  console.log("sign up clicked " + this.email + ' ' + this.name + ' ' + this.password);
  this.authService.signup(this.email,this.password)
    .then(value => {
      this.authService.updateProfile(value.user,this.name);
      }).then(value => {
        this.router.navigate(['/'])
      }).catch(err => {
        this.error = err;
        console.log(err);
        })
}

}
